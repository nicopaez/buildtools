Build Tools v5
==============

This package contains a set of tools commonly used in .Net projects:

* NuGet
* FxCop
* NUnit
* OpenCover
* MSBuildCommunityTask
* AWSS3Uploader

The goal of this package is to simplify the setup and execution of the build process.
The ideal strategy would be to get all this packages with using NuGet but some of this packages are not available on NuGet and because of licenses restrictions we cannot add them. In the future we could add all this components to our private NuGet repository.

You can see _buildScriptSample.xml_ as an example of how to use this package.

This sample script assumes the BuildTools are located under _C:\BuildTools_

**Note 1**: after installing this package the nuget folder must be added the path environment variable.

**Note 2**: before using OpenCover, it should registered in the system by running the command: _regsvr32 .\OpenCover.Profiler.dll_
